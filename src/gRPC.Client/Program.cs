﻿using System;
using System.Threading.Tasks;
using CustomerGrpcServer;
using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;

namespace GrpcClient
{
    internal class Program
    {
        private static async Task Main()
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
        
            var customerClient = new Customer.CustomerClient(channel);

            var response = customerClient.GetCustomers(new Empty());

            foreach (var customer in response.Customers)
                Console.WriteLine($"Customer ({customer.Id}): {customer.FirstName} {customer.LastName} ({customer.Email})");

        }
    }
}