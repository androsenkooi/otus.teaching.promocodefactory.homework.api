﻿using System;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL;

public class CustomerSchema : Schema
{
    public CustomerSchema(IServiceProvider resolver) : base(resolver)
    {
        Query = resolver.GetService<CustomerQuery>();
    }
}
