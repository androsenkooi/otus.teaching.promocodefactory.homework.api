﻿using GraphQL.Types;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL;

public class CustomerType : ObjectGraphType<Customer>
{
    public CustomerType()
    {
        Field(a => a.Id);
        Field(a => a.FirstName);
        Field(a => a.LastName);
        Field(a => a.FullName);
        Field(a => a.Email);
    }
}
